<?php

use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;


Route::get('users/{id}', [UserController::class,'show']);

// Api Route Show Id Node 👆

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
