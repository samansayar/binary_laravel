<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{

    public function show($id)
    {
        $find=User::find($id);
        $get=User::where('ref',$find->user_code)->get();
        return $get;
    }

}
